package com.kairos

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.tripDetail.TripDetailsActvity
import com.kairos.trips.TripModel
import com.kairos.trips.TripsActivity
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.mikelau.croperino.Croperino
import com.mikelau.croperino.CroperinoConfig
import com.mikelau.croperino.CroperinoFileUtil
import kotlinx.android.synthetic.main.activity_add_odometer.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.SocketTimeoutException

/**
 * Created by Naresh Ravva on 5/24/2020.
 */
class AddOdometer : AppCompatActivity() {

    var filePath: File = File("")

    var HAVING_FILE_DATA = 0

    var tripModel = TripModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_odometer)

//        val policy: StrictMode.ThreadPolicy = Builder().permitAll().build()
//        StrictMode.setThreadPolicy(policy)

        tripModel = intent.getSerializableExtra("TRIP_DATA") as TripModel

        rlImageUpload.setOnClickListener {
            //Initialize on every usage
            CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/MikeLau/Pictures", "/sdcard/MikeLau/Pictures")
            CroperinoFileUtil.verifyStoragePermissions(this@AddOdometer)
            CroperinoFileUtil.setupDirectory(this@AddOdometer)
            //Prepare Camera
            Croperino.prepareCamera(this@AddOdometer)
        }

        rlBack.setOnClickListener {
            finish()
        }

        btnSubmit.setOnClickListener {
            if(intent.getBooleanExtra("FROM_END_TRIP",false)){
                endTrip(filePath)
            }else{
                uploadFileStartTrip(filePath)
            }
        }

        if(intent.getBooleanExtra("FROM_END_TRIP",false)){
            btnSubmit.text = "End Trip"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Do saving / uploading of photo method here.
        //The image file can always be retrieved via CroperinoFileUtil.getTempFile()
        when (requestCode) {
            CroperinoConfig.REQUEST_TAKE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    /* Parameters of runCropImage = File, Activity Context, Image is Scalable or Not, Aspect Ratio X, Aspect Ratio Y, Button Bar Color, Background Color */
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@AddOdometer, true, 1, 1, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, this@AddOdometer);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@AddOdometer, true, 0, 0, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_CROP_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val i = Uri.fromFile(CroperinoFileUtil.getTempFile())
                    imgInvoice.setImageURI(i)
                    HAVING_FILE_DATA = 1
                    filePath = File(i.getPath())
                    //Do saving uploading of photo method here. The image file can always be retrieved via CroperinoFileUtil.getTempFile()
                }
            }
        }
    }


    private fun endTrip(file: File) {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to End this Trip?").setTitle("Confirmation")
        builder.apply {
            setPositiveButton("Yes") { dialog, id ->
                uploadFileEndTrip(file)
            }
            setNegativeButton("No") { dialog, id ->
                dialog.dismiss()
            }
        }
        val alert = builder.create()
        alert.show()
    }

    private fun uploadFileEndTrip(file: File) {
        AppUtils.showDialog(this@AddOdometer)

        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("photo", file.name, requestBody)
        val filename = RequestBody.create(MediaType.parse("text/plain"), file.name)

        val apiService: ApiInterface = ApiClientJSON.getClient(this).create(ApiInterface::class.java)

        val tripId = RequestBody.create(MediaType.parse("text/plain"), tripModel.id)
        val reading = RequestBody.create(MediaType.parse("text/plain"), inputPrice.text.toString())
        val orderID = RequestBody.create(MediaType.parse("text/plain"), "1")

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val call = apiService.endTrip(token, tripId, reading, orderID, fileToUpload)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.code() == 503) {
                        AppUtils.showAlert(this@AddOdometer, getString(R.string.server_down_message))
                    }else {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)
                        if (response.code() == 200) {
                            AppUtils.showToast(this@AddOdometer, "Success")
                            val i = Intent(this@AddOdometer, TripsActivity::class.java)
                            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(i)
                            finish()
                        } else {
                            AppUtils.showAlert(this@AddOdometer, "Trip End Not Happen, Please Try again later")
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun uploadFileStartTrip(file: File) {

        if (inputPrice.text.toString().trim().isEmpty()) {
            AppUtils.showAlert(this@AddOdometer, "Enter Odometer Reading")
            return
        }
        AppUtils.showDialog(this@AddOdometer)

        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("photo", file.name, requestBody)
        val filename = RequestBody.create(MediaType.parse("text/plain"), file.name)

        val apiService: ApiInterface = ApiClientJSON.getClient(this).create(ApiInterface::class.java)

        val tripId = RequestBody.create(MediaType.parse("text/plain"), tripModel.id)
        val reading = RequestBody.create(MediaType.parse("text/plain"), inputPrice.text.toString())
        val orderId = RequestBody.create(MediaType.parse("text/plain"), "1")

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val call = apiService.startTrip(token, tripId, reading, orderId, fileToUpload)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val res = response.body()!!.string()
                    val jsonObj = JSONObject(res)

                    if (response.code() == 200) {
                        val intent = Intent(this@AddOdometer, TripDetailsActvity::class.java)
                        intent.putExtra("Reading", inputPrice.text.toString())
                        intent.putExtra("FROM_ODAMETER", true)
                        intent.putExtra("TRIP_DATA", tripModel)
                        startActivity(intent)
                        finish()
                        AppUtils.showToast(this@AddOdometer, "Success")
                    } else if (response.code() == 503) {
                        AppUtils.showAlert(this@AddOdometer, getString(R.string.server_down_message))
                    }else {
                        AppUtils.showAlert(this@AddOdometer, "Fail to Submit")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
                if (t is SocketTimeoutException) {
                    AppUtils.showAlert(this@AddOdometer, "Socket Time out. Please try again.")
                }
            }
        })
    }

}