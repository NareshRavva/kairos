package com.kairos.tripDetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kairos.AddOdometer;
import com.kairos.Constants;
import com.kairos.R;
import com.kairos.trips.TripLocationsModel;
import com.kairos.utils.AppConstants;
import com.kairos.utils.AppUtils;
import com.kairos.utils.KariosConstants;

import java.util.ArrayList;


public class TripsConfirmationAdapter extends RecyclerView.Adapter<TripsConfirmationAdapter.ViewHolder> {

    ArrayList<TripLocationsModel> arrList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private static final String TAG = TripsConfirmationAdapter.class.getName();

    public TripsConfirmationAdapter(Context context, ArrayList<TripLocationsModel> arrList) {
        this.mInflater = LayoutInflater.from(context);
        this.arrList = arrList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_confirmation, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TripLocationsModel model = arrList.get(position);

        holder.txtFrom.setText(model.getCFrom());
        holder.txtTo.setText(model.getCTo());

        if (KariosConstants.isSelectedTrips!=null && KariosConstants.isSelectedTrips.contains(model.getITripDetailsID())) holder.isTripChecked.setChecked(true);
        else holder.isTripChecked.setChecked(false);

        holder.isTripChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) KariosConstants.isSelectedTrips.add(model.getITripDetailsID());
                else KariosConstants.isSelectedTrips.remove(model.getITripDetailsID());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtFrom, txtTo;
        public CheckBox isTripChecked;

        public ViewHolder(View itemView) {
            super(itemView);
            txtFrom = itemView.findViewById(R.id.txtFrom);
            txtTo = itemView.findViewById(R.id.txtTo);
            isTripChecked = itemView.findViewById(R.id.isTripChecked);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.itemClick(view, getAdapterPosition(), "");
        }
    }

    public TripLocationsModel getItem(int id) {
        return arrList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void itemClick(View view, int position, String type);
    }
}