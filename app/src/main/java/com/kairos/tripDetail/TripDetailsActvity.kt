package com.kairos.tripDetail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.kairos.R
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.destination.AddDestination
import com.kairos.destination.DestinationAdapter
import com.kairos.expenses.AddExpenses
import com.kairos.expenses.ExpensesAdapter
import com.kairos.trips.TripLocationsModel
import com.kairos.trips.TripModel
import com.kairos.utils.*
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_trip_details.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Naresh Ravva on 5/8/2020.
 */
class TripDetailsActvity : AppCompatActivity(), View.OnClickListener, ExpensesAdapter.ItemClickListener {

    var arr_trips = ArrayList<ExpenesesDataModel>()

    private var expenses_adapter: ExpensesAdapter? = null
    private var destination_adapter: DestinationAdapter? = null
    var arrLocations: ArrayList<TripLocationsModel> = ArrayList()

    var tripModel = TripModel()

    var expAmount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_details)

        tripModel = intent.getSerializableExtra("TRIP_DATA") as TripModel

        txtDestination.setOnClickListener(this)
        txtExpenses.setOnClickListener(this)
        rlPlus.setOnClickListener(this)

        txtReading.text = "" + tripModel.iOdometerStartReading

        if (intent.getBooleanExtra("FROM_ODAMETER", false)) {
            txtReading.text = intent.getStringExtra("Reading")
        }

        if (tripModel != null && tripModel.arrLocations.size > 0) {
            arrLocations = tripModel.arrLocations
            setDestinationAdapter(arrLocations)
        }

        llBack.setOnClickListener {
            finish()
        }

        getExpensesList(false)

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = false
            if (txtlable.text.toString() == "Expenses") {
                getExpensesList(true)
            }
        }

        btnEndTrip.setOnClickListener {
            val intent = Intent(this, TripConfirmationActivity::class.java)
            intent.putExtra("TRIP_DATA", tripModel)
            intent.putExtra("EXPENSES", txtExpTotalAmount.text.toString())
            intent.putExtra("READING", txtReading.text.toString())
            startActivity(intent)
        }

        if (intent.getBooleanExtra("FROM_COMPLETED", false)) {
            rlPlus.visibility = View.INVISIBLE
            btnEndTrip.visibility = View.GONE
            txtTripStatus.setText("COMPLETED")
            txtTripStatus.setTextColor(ContextCompat.getColor(this, R.color.B400))
        }
    }

    override fun onResume() {
        super.onResume()

        if(arrLocations!=null && arrLocations.size>0 && KariosConstants.DO_REFRESH==true){
            if(KariosConstants.LOC_TYPE.equals("IS_PICKUP")){
                arrLocations[KariosConstants.UPDATE_LOC_POSITION]. cFrom = KariosConstants.FROM_LOC_NAME
            }else if(KariosConstants.LOC_TYPE.equals("IS_DROP")){
                arrLocations[KariosConstants.UPDATE_LOC_POSITION].cTo = KariosConstants.TO_LOC_NAME
            }

            KariosConstants.DO_REFRESH==false
            KariosConstants.LOC_TYPE = ""

            destination_adapter!!.notifyDataSetChanged()

            KariosConstants.DO_REFRESH_LIST = true
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.txtDestination -> {
                txtDestination.background = ContextCompat.getDrawable(applicationContext, R.drawable.destination_bg);
                txtExpenses.background = ContextCompat.getDrawable(applicationContext, R.drawable.expenses_bg);

                txtDestination.setTextColor(ContextCompat.getColor(applicationContext, R.color.white))
                txtExpenses.setTextColor(ContextCompat.getColor(applicationContext, R.color.G500))
                txtlable.setText("Destinations")

                setDestinationAdapter(arrLocations)
            }
            R.id.txtExpenses -> {
                txtDestination.background = ContextCompat.getDrawable(applicationContext, R.drawable.expenses_bg);
                txtExpenses.background = ContextCompat.getDrawable(applicationContext, R.drawable.destination_bg);

                txtDestination.setTextColor(ContextCompat.getColor(applicationContext, R.color.G500))
                txtExpenses.setTextColor(ContextCompat.getColor(applicationContext, R.color.white))
                txtlable.setText("Expenses")
                setExpDataAdapter(arr_trips)

                if(arr_trips.size==0){
                    getExpensesList(true)
                }
            }
            R.id.rlPlus -> {
                if (txtlable.text.toString() == "Expenses") {
                    val intent = Intent(this, AddExpenses::class.java)
                    intent.putExtra("TRIP_DATA", tripModel)
                    startActivityForResult(intent, 100)
                } else {
                    val intent = Intent(this, AddDestination::class.java)
                    intent.putExtra("TRIP_DATA", tripModel)
                    startActivityForResult(intent, 101)
                }
            }
        }
    }


    private fun getExpensesList(isFlag: Boolean) {
        arr_trips.clear()
        arr_trips = ArrayList()

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.GET_EXPENSES

        val jsonObj = JSONObject()
        try {
            jsonObj.put("tripId", tripModel.id)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val call = apiService.getExpenseList(service_url, token, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.code() == 503) {
                        AppUtils.showAlert(this@TripDetailsActvity, getString(R.string.server_down_message))
                    }else {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)

                        swipeRefreshLayout.isRefreshing = false

                        if (response.code() == 200) {
                            val expArr = jsonObj.optJSONArray("data")
                            expAmount = 0
                            if (expArr != null) {
                                for (i in 0..expArr.length() - 1) {
                                    val expObject = expArr.optJSONObject(i)
                                    expAmount += expObject.optString("iExpenseCost").toInt()
                                    arr_trips.add(
                                        ExpenesesDataModel(
                                            expObject.optInt("iExpenseID"), expObject.optString("iTripID"),
                                            expObject.optString("cExpenseType"), expObject.optString("iExpenseCost"),
                                            expObject.optString("cExpenseImage"), expObject.optString("datetime")
                                        )
                                    )
                                }
                                txtExpTotalAmount.text = "₱ " + expAmount
                            } else {
                                txtExpTotalAmount.text = "₱ 0.00"
                            }
                            if (isFlag) setExpDataAdapter(arr_trips)
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun setExpDataAdapter(list: java.util.ArrayList<ExpenesesDataModel>) {
        if (list.size > 0) {
            rvData.layoutManager = LinearLayoutManager(this)
            expenses_adapter = ExpensesAdapter(this, list,intent.getBooleanExtra("FROM_COMPLETED", false))
            expenses_adapter!!.setClickListener(this)
            rvData.adapter = expenses_adapter
            txt_nodata.visibility = View.GONE
            swipeRefreshLayout.visibility = View.VISIBLE
        } else {
            txt_nodata.visibility = View.VISIBLE
            swipeRefreshLayout.visibility = View.GONE
        }
    }

    private fun setDestinationAdapter(list: java.util.ArrayList<TripLocationsModel>) {
        if (list.size > 0) {
            rvData.layoutManager = LinearLayoutManager(this)
            destination_adapter = DestinationAdapter(this, list)
            //destination_adapter!!.setClickListener(this)
            rvData.adapter = destination_adapter
            txt_nodata.visibility = View.GONE
            swipeRefreshLayout.visibility = View.VISIBLE
        } else {
            txt_nodata.visibility = View.VISIBLE
            swipeRefreshLayout.visibility = View.VISIBLE
        }
    }

    override fun itemClick(view: View?, model: ExpenesesDataModel, type: String?) {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to delete this expense?").setTitle("Confirmation")
        builder.apply {
            setPositiveButton("Yes") { dialog, id ->
                deleteExpense(model)
            }
            setNegativeButton("No") { dialog, id ->
                dialog.dismiss()
            }
        }
        val alert = builder.create()
        alert.show()
    }

    private fun deleteExpense(model: ExpenesesDataModel) {
        val jsonObj = JSONObject()
        try {
            jsonObj.put("tripId", model.iTripID)
            jsonObj.put("expenseId", model.iExpenseID)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.DELETE_EXPENSE
        val call = apiService.deleteExpenses(service_url, token, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.code() == 200) {
                        AppUtils.showToast(this@TripDetailsActvity, "Success")
                        getExpensesList(true)
                    } else if (response.code() == 503) {
                        AppUtils.showAlert(this@TripDetailsActvity, getString(R.string.server_down_message))
                    }else {
                        AppUtils.showAlert(this@TripDetailsActvity, "Fail to Delete")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            101 -> {
                if (resultCode == Activity.RESULT_OK) {
                    val tripLocationsModel = data!!.getSerializableExtra("DATA") as TripLocationsModel
                    arrLocations.add(tripLocationsModel)
                    setDestinationAdapter(arrLocations)
                    AppUtils.showToast(this, "Destination Added Successfully")
                }
            }
            100 -> {
                if (resultCode == Activity.RESULT_OK) {
                    AppUtils.showAlert(this, "Expense Added Successfully")
                    val is_refresh = data!!.getBooleanExtra("REFRESH_DATA", false)
                    if (is_refresh)
                        getExpensesList(true)
                }
            }
        }
    }


}