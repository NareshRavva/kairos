package com.kairos.tripDetail

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.gcacace.signaturepad.views.SignaturePad
import com.kairos.AddOdometer
import com.kairos.R
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.trips.TripLocationsModel
import com.kairos.trips.TripModel
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.mikelau.croperino.Croperino
import com.mikelau.croperino.CroperinoConfig
import com.mikelau.croperino.CroperinoFileUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_trip_confirmation.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream


/**
 * Created by Naresh Ravva on 5/7/2020.
 */
class TripConfirmationActivity : AppCompatActivity() {
    private var user_accounts_adapter: TripsConfirmationAdapter? = null

    var tripModel = TripModel()

    var filePath: File = File("")

    var HAVING_FILE_DATA = 0

    var HAVING_SIGN_FILE_DATA = 0


    override fun onSaveInstanceState(outState: Bundle) {
        try {//Clear the Activity's bundle of the subsidiary fragments' bundles.
            super.onSaveInstanceState(outState)
            outState.clear()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_confirmation)

        tripModel = intent.getSerializableExtra("TRIP_DATA") as TripModel

        txtExpTotalAmount.text = intent.getStringExtra("EXPENSES")
        txtReading.text = intent.getStringExtra("READING")

        setDataAdapter(tripModel.arrLocations)

        rlBack.setOnClickListener{
            finish()
        }

        btnEndTrip.setOnClickListener {
            saveSignature(false)
        }

        txtSave.setOnClickListener{
            saveSignature(true)
        }

        rlImageUpload.setOnClickListener {
            //Initialize on every usage
            CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/MikeLau/Pictures", "/sdcard/MikeLau/Pictures")
            CroperinoFileUtil.verifyStoragePermissions(this)
            CroperinoFileUtil.setupDirectory(this)
            //Prepare Camera
            Croperino.prepareCamera(this)
        }

        txtUndo.setOnClickListener{
            signature_pad.clear()
        }

        signature_pad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                //Event triggered when the pad is touched
            }

            override fun onSigned() {
                HAVING_SIGN_FILE_DATA = 1
            }

            override fun onClear() {
                //Event triggered when the pad is cleared
            }
        })

        if(AppConstants.CUSTOMER_SIGNATURE.length>5) {
            Log.v("TripConfimation", "--cCustomerSignImage-->" + AppConstants.CUSTOMER_SIGNATURE)
            Picasso.get().load(AppConstants.CUSTOMER_SIGNATURE).into(imgSignature)
            txtsignTime.text = AppConstants.CUSTOMER_SIGNATURE_TIME
            rlSign.visibility = View.VISIBLE
            signature_pad.visibility = View.GONE
            txtSigData.visibility = View.VISIBLE
            txtUndo.visibility = View.GONE
            txtSave.visibility = View.GONE
        }
    }

    private fun saveSignature(isFrom: Boolean) {
        if(AppConstants.CUSTOMER_SIGNATURE.length>5) {
            val i = Intent(this@TripConfirmationActivity, AddOdometer::class.java)
            i.putExtra("FROM_END_TRIP", true)
            i.putExtra("TRIP_DATA", tripModel)
            startActivity(i)
        }else {
            val bitmap = signature_pad.signatureBitmap
            val bos = ByteArrayOutputStream()
            bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
            val bitmapdata: ByteArray = bos.toByteArray()

            val file = File(getCacheDir(), "temporary_file.jpg")

            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()

            if (HAVING_SIGN_FILE_DATA == 0) {
                AppUtils.showAlert(this, "Required Customer Signature")
            } else {
                //endTrip(file)
                uploadFile(file, isFrom)
            }
        }
    }

    private fun setDataAdapter(list: java.util.ArrayList<TripLocationsModel>) {
        if (list.size > 0) {
            rvData.setLayoutManager(LinearLayoutManager(this))
            user_accounts_adapter = TripsConfirmationAdapter(this, list)
            rvData.setAdapter(user_accounts_adapter)
        }
    }

    private fun uploadFile(file: File, isFromSig: Boolean) {
        AppUtils.showDialog(this@TripConfirmationActivity)
        val requestBody_sign = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload_sign = MultipartBody.Part.createFormData("signPhoto", file.name, requestBody_sign)

        val requestBody = RequestBody.create(MediaType.parse("*/*"), filePath)
        val fileToUpload = MultipartBody.Part.createFormData("photo", filePath.name, requestBody)

        val apiService: ApiInterface = ApiClientJSON.getClient(this).create(ApiInterface::class.java)

        val tripId = RequestBody.create(MediaType.parse("text/plain"), tripModel.id)
        val reading = RequestBody.create(MediaType.parse("text/plain"), etxtFinalOdomater.text.toString())
        val orderID = RequestBody.create(MediaType.parse("text/plain"), "1")

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val call = apiService.updateEndTripStatus(token, tripId, fileToUpload_sign)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.code() == 503) {
                        AppUtils.showAlert(this@TripConfirmationActivity, getString(R.string.server_down_message))
                    } else {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)
                        if (response.code() == 200) {
                            AppUtils.showToast(this@TripConfirmationActivity, "Success")
                            if (isFromSig) {
                                txtSigData.visibility = View.VISIBLE
                                txtUndo.visibility = View.GONE
                                txtSave.visibility = View.GONE

                                val tripsArr = jsonObj.optJSONArray("tripsData")

                                for (i in 0..tripsArr.length() - 1) {
                                    val tripObjt = tripsArr.optJSONObject(i)
                                    val cCustomerSignImage = tripObjt.optString("cCustomerSignImage")
                                    AppConstants.CUSTOMER_SIGNATURE = cCustomerSignImage;
                                    Log.v("TripConfimation", "--cCustomerSignImage-->" + cCustomerSignImage)
                                    Picasso.get().load(cCustomerSignImage).into(imgSignature)
                                    rlSign.visibility = View.VISIBLE
                                    signature_pad.visibility = View.GONE

                                    val dtCustomerSignOn = tripObjt.optString("dtCustomerSignOn")
                                    txtsignTime.setText(AppUtils.parseSigTime(dtCustomerSignOn))
                                    AppConstants.CUSTOMER_SIGNATURE_TIME = AppUtils.parseSigTime(dtCustomerSignOn)
                                }
                            } else {
                                val i = Intent(this@TripConfirmationActivity, AddOdometer::class.java)
                                i.putExtra("FROM_END_TRIP", true)
                                i.putExtra("TRIP_DATA", tripModel)
                                startActivity(i)
                            }
                        } else {
                            AppUtils.showAlert(this@TripConfirmationActivity, "Trip End Not Happen, Please Try again later")
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

//    private fun endTrip(file: File) {
//        val builder = android.app.AlertDialog.Builder(this)
//        builder.setMessage("Are you sure you want to End this Trip?").setTitle("Confirmation")
//        builder.apply {
//            setPositiveButton("Yes") { dialog, id ->
//                uploadFile(file)
//            }
//            setNegativeButton("No") { dialog, id ->
//                dialog.dismiss()
//            }
//        }
//        val alert = builder.create()
//        alert.show()
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Do saving / uploading of photo method here.
        //The image file can always be retrieved via CroperinoFileUtil.getTempFile()
        when (requestCode) {
            CroperinoConfig.REQUEST_TAKE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    /* Parameters of runCropImage = File, Activity Context, Image is Scalable or Not, Aspect Ratio X, Aspect Ratio Y, Button Bar Color, Background Color */
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@TripConfirmationActivity, true, 1, 1, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, this@TripConfirmationActivity);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@TripConfirmationActivity, true, 0, 0, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_CROP_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val i = Uri.fromFile(CroperinoFileUtil.getTempFile())
                    imgInvoice.setImageURI(i)
                    HAVING_FILE_DATA = 1
                    filePath = File(i.getPath())
                    //Do saving uploading of photo method here. The image file can always be retrieved via CroperinoFileUtil.getTempFile()
                }
            }
        }
    }
}