package com.kairos.tripDetail

/**
 * Created by Naresh Ravva on 5/25/2020.
 */
class ExpenesesDataModel(
    var iExpenseID: Int = 0,
    var iTripID: String = "",
    var cExpenseType: String = "",
    var iExpenseCost: String = "",
    var cExpenseImage: String = "",
    var datetime: String = ""
)