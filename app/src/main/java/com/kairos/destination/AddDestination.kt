package com.kairos.destination

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.kairos.R
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.trips.TripLocationsModel
import com.kairos.trips.TripModel
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.kairos.utils.KairosUtils
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_completed_trips.*
import kotlinx.android.synthetic.main.activity_destination.*
import kotlinx.android.synthetic.main.activity_destination.rlBack
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * Created by Naresh Ravva on 5/31/2020.
 */
class AddDestination : AppCompatActivity() {

    var placesClient: PlacesClient? = null

    var adapter: AutoCompleteAdapter? = null

    var tripModel = TripModel()

    var tripID: String = ""
    var orderId: String = ""
    var from: String = ""
    var fromLatLng: String = ""
    var fromPlaceId: String = ""
    var toLoc: String = ""
    var toLatLng: String = ""
    var toPlaceId: String = ""
    var lastObject = TripLocationsModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination)

        tripModel = intent.getSerializableExtra("TRIP_DATA") as TripModel

        val apiKey = getString(R.string.api_key)

        // Setup Places Client

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }

        placesClient = Places.createClient(this)
        initAutoCompleteTextView()

        autoCompleteTextView.setTypeface(Typeface.createFromAsset(assets, "fonts/FuturaPT-Book.otf"))

        lastObject = tripModel.arrLocations[tripModel.arrLocations.size - 1]
        etxtPreviousLocation.setText(lastObject.cTo)

        tripID = lastObject.iTripID
        orderId = lastObject.iOrder
        from = lastObject.cTo
        fromLatLng = lastObject.cToLatLng
        fromPlaceId = lastObject.cToPlaceId

        rlBack.setOnClickListener{
            finish()
        }

        btnSubmit.setOnClickListener {
            addDestination();
        }
    }

    private fun addDestination() {
        val jsonObj = JSONObject()
        try {
            jsonObj.put("tripId", tripID)
            jsonObj.put("orderId", orderId.toInt() + 1)
            jsonObj.put("from", from)
            jsonObj.put("fromLatLng", fromLatLng)
            jsonObj.put("fromPlaceId", fromPlaceId)
            jsonObj.put("to", autoCompleteTextView.text.toString())
            jsonObj.put("toLatLng", toLatLng)
            jsonObj.put("toPlaceId", toPlaceId)
            jsonObj.put("reading",etxtAddOdometer.text.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.ADD_DESTINATION
        val call = apiService.addDestination(service_url, token, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {

                    if (response.code() == 200) {
                        val res = response.body()!!.string()
                        val tripLocModel = TripLocationsModel()
                        tripLocModel.iTripDetailsID = lastObject.iTripDetailsID
                        tripLocModel.iTripID = lastObject.iTripID
                        tripLocModel.cFrom = from
                        tripLocModel.cFromLatLng = fromLatLng
                        tripLocModel.cFromPlaceId = fromPlaceId
                        tripLocModel.cTo = autoCompleteTextView.text.toString()
                        tripLocModel.cToLatLng = toLatLng
                        tripLocModel.cToPlaceId = toPlaceId
                        tripLocModel.iOrder = ""+orderId.toInt() + 1
                        tripLocModel.dtPickupDateTime = AppUtils.currentTime()
                        tripLocModel.dtDropDateTime=""

                        val intent = Intent()
                        intent.putExtra("DATA",tripLocModel)
                        setResult(Activity.RESULT_OK, intent);
                        finish();

                        //AppUtils.showAlert(this@AddDestination, "Destination Added Successfully")
                    } else if (response.code() == 503) {
                        AppUtils.showAlert(this@AddDestination, getString(R.string.server_down_message))
                    }else {
                        AppUtils.showAlert(this@AddDestination, "Fail to Added")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun initAutoCompleteTextView() {
        autoCompleteTextView.threshold = 1
        autoCompleteTextView.onItemClickListener = autocompleteClickListener
        adapter = AutoCompleteAdapter(this, placesClient)
        autoCompleteTextView.setAdapter(adapter)
    }

    private val autocompleteClickListener = OnItemClickListener { adapterView, view, i, l ->
        try {
            val item = adapter!!.getItem(i)
            var placeID: String? = null
            if (item != null) {
                placeID = item.placeId
            }

//          To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
//          Use only those fields which are required.
            val placeFields = Arrays.asList(
                Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG
            )
            var request: FetchPlaceRequest? = null
            if (placeID != null) {
                request = FetchPlaceRequest.builder(placeID, placeFields)
                    .build()
            }
            if (request != null) {
                placesClient!!.fetchPlace(request).addOnSuccessListener { task ->
                    //responseView.setText("""${task.place.name}${task.place.address}""".trimIndent())
                    toPlaceId = task.place.id!!

                    val latLng: LatLng = task.place.latLng!!
                    toLatLng = latLng.latitude.toString() + "," + latLng.longitude.toString()
                    AppUtils.hideKeyBoard(this,etxtPreviousLocation)

                }.addOnFailureListener { e ->
                    e.printStackTrace()
                    //responseView.setText(e.message)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}