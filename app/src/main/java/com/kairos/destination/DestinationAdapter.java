package com.kairos.destination;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kairos.R;
import com.kairos.trips.TripLocationsModel;
import com.kairos.utils.AppUtils;
import com.kairos.utils.KariosConstants;

import java.util.ArrayList;


public class DestinationAdapter extends RecyclerView.Adapter<DestinationAdapter.ViewHolder> {

    ArrayList<TripLocationsModel> arrList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private static final String TAG = DestinationAdapter.class.getName();

    public DestinationAdapter(Context context, ArrayList<TripLocationsModel> arrList) {
        this.mInflater = LayoutInflater.from(context);
        this.arrList = arrList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_destination, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TripLocationsModel model = arrList.get(position);

        holder.txtPickup.setText(model.getCFrom());
        holder.txtDrop.setText(model.getCTo());
        holder.txtDate.setText(AppUtils.parseTime(model.getDtPickupDateTime()));
        holder.txtPosition.setText(""+(position + 1));
        KariosConstants.UPDATE_LOC_POSITION = position;

        holder.txtPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KariosConstants.LOC_TYPE = "IS_PICKUP";
                Intent intent = new Intent(context, UpdateDestination.class);
                intent.putExtra("IS_PICKUP", true);
                intent.putExtra("FROM_DEST_ADAPTER", true);
                intent.putExtra("TRIP_LOCATION_DATA", model);
                context.startActivity(intent);
            }
        });

        holder.txtDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KariosConstants.LOC_TYPE = "IS_DROP";
                Intent intent = new Intent(context, UpdateDestination.class);
                intent.putExtra("IS_DROP", true);
                intent.putExtra("FROM_DEST_ADAPTER", true);
                intent.putExtra("TRIP_LOCATION_DATA", model);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtPickup, txtDrop, txtDate, txtPosition;

        public ViewHolder(View itemView) {
            super(itemView);
            txtPickup = itemView.findViewById(R.id.txtPickup);
            txtDrop = itemView.findViewById(R.id.txtDrop);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtPosition = itemView.findViewById(R.id.txtPosition);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.itemClick(view, getAdapterPosition(), "");
        }
    }

    public TripLocationsModel getItem(int id) {
        return arrList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void itemClick(View view, int position, String type);
    }
}