package com.kairos

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_contactus.*

/**
 * Created by Naresh Ravva on 6/5/2020.
 */
class ContactUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contactus)

        rlBack.setOnClickListener {
            finish()
        }
    }
}