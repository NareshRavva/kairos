package com.kairos.trips;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kairos.AddOdometer;
import com.kairos.R;
import com.kairos.destination.UpdateDestination;
import com.kairos.tripDetail.TripDetailsActvity;
import com.kairos.utils.AppConstants;
import com.kairos.utils.AppUtils;

import java.util.ArrayList;


public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    ArrayList<TripModel> arrList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private static final String TAG = TripsAdapter.class.getName();
    private Boolean isFlag;

    public TripsAdapter(Context context, ArrayList<TripModel> arrList, Boolean isFlag) {
        this.mInflater = LayoutInflater.from(context);
        this.arrList = arrList;
        this.context = context;
        this.isFlag = isFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_trip, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TripModel model = arrList.get(position);

        holder.txtPickUpTime.setText(AppUtils.parseTime(model.getDate()));
        holder.txtPickUpLocation.setText(model.getPickupLocation());
        holder.txtCustomerName.setText(model.getCustomerName());
        holder.txtDate.setText(AppUtils.parseDate(model.getDate()));

        if(model.getCCompanyName()!=null && model.getCCompanyName().length()>1){
            holder.txtCompanyName.setText(model.getCCompanyName());
            holder.llCompanyLable.setVisibility(View.VISIBLE);
        }else{
            holder.llCompanyLable.setVisibility(View.INVISIBLE);
        }

        if (model.getIOdometerStartReading() == 0) holder.txtStartTrip.setText("START TRIP");
        else holder.txtStartTrip.setText("VIEW TRIP");

        if (isFlag) holder.txtStatus.setVisibility(View.VISIBLE);
        else holder.txtStatus.setVisibility(View.GONE);

        holder.rlChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", model.getMobile());
                    smsIntent.putExtra("sms_body", "Hello...");
                    context.startActivity(smsIntent);
                } catch (Exception e) {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setData(Uri.parse("sms:"));
                    smsIntent.putExtra("address", model.getMobile());
                    smsIntent.putExtra("sms_body", "Hello...");
                    context.startActivity(smsIntent);
                    e.printStackTrace();
                }
            }
        });

        holder.rlPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + model.getMobile()));
                context.startActivity(intent);
            }
        });

        holder.llListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getIEndTripStatus() == 1) {
                    Intent i = new Intent(context, AddOdometer.class);
                    i.putExtra("FROM_END_TRIP", true);
                    i.putExtra("TRIP_DATA", model);
                    context.startActivity(i);
                } else {
                    Intent intent = new Intent(context, TripDetailsActvity.class);
                    if (model.getIOdometerStartReading() == 0) {
                        intent = new Intent(context, AddOdometer.class);
                    }
                    intent.putExtra("TRIP_DATA", model);
                    if (isFlag) intent.putExtra("FROM_COMPLETED", true);
                    context.startActivity(intent);
                }
            }
        });

        holder.llUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateDestination.class);
                intent.putExtra("TRIP_DATA", model);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtPickUpTime, txtPickUpLocation, txtCustomerName, txtDate, txtStartTrip, txtStatus,txtCompanyName;
        public RelativeLayout rlPhone, rlChat;
        public LinearLayout llListItem,llCompanyLable,llUpdateLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            txtPickUpTime = itemView.findViewById(R.id.txtPickUpTime);
            txtPickUpLocation = itemView.findViewById(R.id.txtPickUpLocation);
            txtCustomerName = itemView.findViewById(R.id.txtCustomerName);
            txtDate = itemView.findViewById(R.id.txtDate);
            llListItem = itemView.findViewById(R.id.llListItem);
            llCompanyLable = itemView.findViewById(R.id.llCompanyLable);
            txtStartTrip = itemView.findViewById(R.id.txtStartTrip);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            txtCompanyName = itemView.findViewById(R.id.txtCompanyName);
            llUpdateLocation = itemView.findViewById(R.id.llUpdateLocation);

            rlPhone = itemView.findViewById(R.id.rlPhone);
            rlChat = itemView.findViewById(R.id.rlChat);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.itemClick(view, getAdapterPosition(), "");
        }
    }

    public TripModel getItem(int id) {
        return arrList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void itemClick(View view, int position, String type);
    }
}