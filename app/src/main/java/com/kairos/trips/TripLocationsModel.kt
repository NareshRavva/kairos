package com.kairos.trips

import java.io.Serializable

/**
 * Created by Naresh Ravva on 5/20/2020.
 */
class TripLocationsModel : Serializable {
    var iTripDetailsID: String = ""
    var iTripID: String = ""
    var cFrom: String = ""
    var cFromLatLng: String = ""
    var cFromPlaceId: String = ""
    var cTo: String = ""
    var cToLatLng: String = ""
    var cToPlaceId: String = ""
    var iOrder: String = ""
    var dtPickupDateTime: String = ""
    var dtDropDateTime: String = ""
}

