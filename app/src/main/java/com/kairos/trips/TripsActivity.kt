package com.kairos.trips

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.iid.FirebaseInstanceId
import com.kairos.*
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.completed.CompletedTripsActivity
import com.kairos.tripDetail.TripDetailsActvity
import com.kairos.utils.*
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_trips.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.include_navigationview.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Naresh Ravva on 5/6/2020.
 */
class TripsActivity : AppCompatActivity(), TripsAdapter.ItemClickListener, View.OnClickListener {

    var trips: ArrayList<TripModel> = ArrayList()

    private var user_accounts_adapter: TripsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trips)

        getTripsData()

        imgSideMenu.setOnClickListener(this)
        rlCompletedTrips.setOnClickListener(this)
        rlContactUs.setOnClickListener(this)
        rlPrivacyPolicy.setOnClickListener(this)
        rlTermsAndConditions.setOnClickListener(this)
        rllogout.setOnClickListener(this)

        val name = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_FIRSTNAME) + " " + CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_LASTNAME)
        txtName.text = name
        txtEmail.text = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_EMAIL)

        swipeRefreshLayout.setOnRefreshListener {
            getTripsData()
        }

        updateTokenToServer()

        try {
            val pInfo: PackageInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0)
            val version = pInfo.versionName
            txtVersion.setText("Version: "+version)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        AppConstants.CUSTOMER_SIGNATURE = ""

        if(KariosConstants.DO_REFRESH_LIST){
            KariosConstants.DO_REFRESH_LIST = false
            getTripsData()
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.rllogout -> logout()
            R.id.rlContactUs -> {
                val intent = Intent(this, ContactUsActivity::class.java)
                startActivity(intent)
            }
            R.id.imgSideMenu -> drawer_layout.openDrawer(GravityCompat.START)

            R.id.rlTermsAndConditions -> moveTowebView("https://portal.kairoscarrental.com/terms", "Terms & Conditions")

            R.id.rlPrivacyPolicy -> moveTowebView("https://portal.kairoscarrental.com/privacy", "Privacy Policy")

            R.id.rlCompletedTrips -> {
                drawer_layout.closeDrawers()
                val intent = Intent(this, CompletedTripsActivity::class.java)
                startActivity(intent)
            }
        }
    }


    private fun moveTowebView(urlPath: String, title: String) {
        val intent = Intent(this, WebViewActivity::class.java)
        intent.putExtra("URL", urlPath)
        intent.putExtra("TITLE", title)
        startActivity(intent)
    }

    private fun getTripsData() {
        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.MY_TRIPS
        val call = apiService.getTrips(service_url, token)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                swipeRefreshLayout.isRefreshing = false
                try {
                    if (response.code() == 401) {
                        //CommonSharedPref.setSharedPrefDataByKey(this@TripsActivity, AppConstants.S_KEY_IS_LOGIN, "0")
                        //moveToLoin()

                        //session expried do login automatically
                        doAuthentication()
                    } else if (response.code() == 404) {
                        AppUtils.showAlert(this@TripsActivity, "Invalid Email/Password")
                    } else if (response.code() == 503) {
                        AppUtils.showAlert(this@TripsActivity, getString(R.string.server_down_message))
                    } else if (response.code() == 200) {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)

                        val tripsArr = jsonObj.optJSONArray("tripsData")

                        if (tripsArr == null) {
                            setDataAdapter(ArrayList())
                            return
                        }

                        trips = ArrayList()
                        for (i in 0..tripsArr.length() - 1) {
                            val tripObjt = tripsArr.optJSONObject(i)

                            val id = tripObjt.optString("iTripID")
                            val date = tripObjt.optString("dtPickupDateTime")
                            val customerName = tripObjt.optString("cFirstName") + " " + tripObjt.optString("cLastName")
                            val mobile = tripObjt.optString("iMobileNumber")
                            var pickUpLocation = ""
                            var dropLocation = ""
                            val iOdometerStartReading = tripObjt.optInt("iOdometerStartReading")
                            val cCompanyName = tripObjt.optString("cCompanyName")
                            val iEndTripStatus = tripObjt.optInt("iEndTripStatus")

                            val tripDetailsArr = tripObjt.optJSONArray("tripDetails")

                            val arrLocation: ArrayList<TripLocationsModel> = ArrayList()
                            var orderID = ""
                            var tripDetailsID = ""

                            for (k in 0..tripDetailsArr.length() - 1) {
                                val tripDetailObjt = tripDetailsArr.optJSONObject(k)
                                pickUpLocation = tripDetailObjt.optString("cFrom")
                                dropLocation = tripDetailObjt.optString("cTo")

                                val tlModel = TripLocationsModel()
                                tlModel.iTripDetailsID = tripDetailObjt.optString("iTripDetailsID")
                                tlModel.iTripID = tripDetailObjt.optString("iTripID")
                                tlModel.cFrom = tripDetailObjt.optString("cFrom")
                                tlModel.cFromLatLng = tripDetailObjt.optString("cFromLatLng")
                                tlModel.cFromPlaceId = tripDetailObjt.optString("cFromPlaceId")
                                tlModel.cTo = tripDetailObjt.optString("cTo")
                                tlModel.cToLatLng = tripDetailObjt.optString("cToLatLng")
                                tlModel.cToPlaceId = tripDetailObjt.optString("cToPlaceId")
                                tlModel.iOrder = tripDetailObjt.optString("iOrder")
                                tlModel.dtPickupDateTime = tripDetailObjt.optString("dtPickupDateTime")
                                tlModel.dtDropDateTime = tripDetailObjt.optString("dtDropDateTime")
                                arrLocation.add(tlModel)

                                orderID = tripDetailObjt.optString("iOrder")
                                tripDetailsID = tripDetailObjt.optString("iTripDetailsID")
                            }

                            val tripModel = TripModel()
                            tripModel.id = id
                            tripModel.date = date
                            tripModel.pickupLocation = pickUpLocation
                            tripModel.dropLocation = dropLocation
                            tripModel.customerName = customerName
                            tripModel.mobile = mobile
                            tripModel.iOdometerStartReading = iOdometerStartReading
                            tripModel.iEndTripStatus = iEndTripStatus
                            tripModel.arrLocations = arrLocation
                            tripModel.cCompanyName = cCompanyName
                            tripModel.iOrder = orderID
                            tripModel.iTripDetailsID = tripDetailsID
                            trips.add(tripModel)
                        }

                        setDataAdapter(trips)

                        Log.v("DATA", "trips-->" + trips.size)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun moveToLoin() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("is_session_exp", true)
        startActivity(intent)
        finish()
    }

    private fun setDataAdapter(list: java.util.ArrayList<TripModel>) {
        if (list.size > 0) {
            rvData.setLayoutManager(LinearLayoutManager(this))
            user_accounts_adapter = TripsAdapter(this, list, false)
            user_accounts_adapter!!.setClickListener(this)
            rvData.setAdapter(user_accounts_adapter)
            llNoRecords.setVisibility(View.GONE)
            rvData.setVisibility(View.VISIBLE)
        } else {
            llNoRecords.setVisibility(View.VISIBLE)
            rvData.setVisibility(View.GONE)
        }
    }

    private fun updateTokenToServer() {
        val token = FirebaseInstanceId.getInstance().token
        Log.i("tripsActivity", "FCM Registration Token: " + token)
        addFirebaseToken(token!!)
    }

    override fun itemClick(view: View?, position: Int, type: String?) {
        var model = trips.get(position)
        //Id trip already updated
        if (model.iEndTripStatus == 1) {
            val i = Intent(this, AddOdometer::class.java)
            i.putExtra("FROM_END_TRIP", true)
            i.putExtra("TRIP_DATA", model)
            startActivity(i)
        } else {
            val intent = Intent(this, TripDetailsActvity::class.java)
            startActivity(intent)
        }
    }

    private fun logout() {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to log out?").setTitle("Log Out")
        builder.apply {
            setPositiveButton("Yes") { dialog, id ->
                CommonSharedPref.setSharedPrefDataByKey(this@TripsActivity, AppConstants.S_KEY_IS_LOGIN, "0")

                val intent = Intent(this@TripsActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()

            }
            setNegativeButton("No") { dialog, id ->
                dialog.dismiss()
            }
        }
        val alert = builder.create()
        alert.show()
    }

    private fun doAuthentication() {
        AppUtils.showDialog(this)

        val jsonObj = JSONObject()

        try {
            jsonObj.put("email", CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_EMAIL))
            jsonObj.put("password", CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_PW))
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.LOGIN
        val call = apiService.login_authenticate(service_url, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {

                    val res = response.body()!!.string()
                    val jsonObj = JSONObject(res)

                    if (response.code() == 401 || jsonObj.optInt("statusCode") == 404) {
                        //AppUtils.showAlert(this@LoginActivity, "Invalid Email/Password")
                    } else if (response.code() == 503) {
                        AppUtils.showAlert(this@TripsActivity, getString(R.string.server_down_message))
                    } else {
                        CommonSharedPref.setSharedPrefDataByKey(this@TripsActivity, AppConstants.S_KEY_TOKEN, jsonObj.optString("token"))
                        CommonSharedPref.setSharedPrefDataByKey(this@TripsActivity, AppConstants.S_KEY_IS_LOGIN, "1")

                        getTripsData()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun addFirebaseToken(firebaseToken: String) {
        val jsonObj = JSONObject()
        try {
            jsonObj.put("driverId", 1)
            jsonObj.put("deviceId", firebaseToken)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.UPDATE_FIREBASE_TOKEN
        val call = apiService.updateFirebaseToken(service_url, token, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
//                    if (response.code() == 200) {
//                        AppUtils.showToast(this@TripsActivity, "Success")
//                        val i = Intent(this@TripsActivity, TripsActivity::class.java)
//                        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                        startActivity(i)
//                        finish()
//                    } else if (response.code() == 503) {
//                        AppUtils.showAlert(this@UpdateDestination, getString(R.string.server_down_message))
//                    }else {
//                        AppUtils.showAlert(this@UpdateDestination, "Fail to Added")
//                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }
}