package com.kairos.trips

import java.io.Serializable

/**
 * Created by Naresh Ravva on 5/20/2020.
 */
class TripModel : Serializable {
    var id: String = ""
    var date: String = ""
    var pickupLocation: String = ""
    var customerName: String = ""
    var mobile: String = ""
    var iOdometerStartReading: Int = 0
    var iEndTripStatus:Int = 0
    var dropLocation: String = ""
    var cCompanyName:String = ""
    var arrLocations: ArrayList<TripLocationsModel> = ArrayList()
    var cCustomerSignImage:String = ""
    var iOrder: String = ""
    var iTripDetailsID: String = ""
}

