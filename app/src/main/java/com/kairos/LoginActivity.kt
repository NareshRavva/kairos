package com.kairos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.trips.TripsActivity
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.kairos.utils.KairosUtils
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var is_login = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_IS_LOGIN)

//        etxtEmail.setText("JR@kairos.com")
//        etxtPassword.setText("test@123")

        if (is_login != null && is_login == "1") {
            moveToTrips()
        }

        btn_login.setOnClickListener {
            doAuthentication()
        }

        val is_session_exp = intent.getBooleanExtra("is_session_exp", false)
        if (is_session_exp) {
            AppUtils.showAlert(this@LoginActivity, "Your current session was timed-out and you have been logged out, Please login again to continue")
        }
    }

    private fun moveToTrips() {
        val intent = Intent(this@LoginActivity, TripsActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun doAuthentication() {
        if (etxtEmail.text.toString().trim().isEmpty()) {
            AppUtils.showToast(this@LoginActivity, "Enter Email")
            return
        }
        if (etxtPassword.getText().toString().trim().isEmpty()) {
            AppUtils.showToast(this@LoginActivity, "Enter Password")
            return
        }

        AppUtils.showDialog(this)

        val jsonObj = JSONObject()
        val android_id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        try {
            jsonObj.put("email", etxtEmail.text.toString())
            jsonObj.put("password", etxtPassword.text.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.LOGIN
        val call = apiService.login_authenticate(service_url, KairosUtils.getRequestBody(jsonObj))

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {

                    val res = response.body()!!.string()
                    val jsonObj = JSONObject(res)

                    if (response.code() == 401 || jsonObj.optInt("statusCode") == 404) {
                        AppUtils.showAlert(this@LoginActivity, "Invalid Email/Password")
                    }
                    else if (response.code() == 503) {
                        AppUtils.showAlert(this@LoginActivity, getString(R.string.server_down_message))
                    }else {
                        AppUtils.showToast(this@LoginActivity, "Login Success")
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_TOKEN, jsonObj.optString("token"))
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_IS_LOGIN, "1")
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_IS_FIRSTNAME, jsonObj.optString("firstName"))
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_IS_LASTNAME, jsonObj.optString("lastName"))
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_IS_EMAIL, etxtEmail.text.toString())
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_IS_PW, etxtPassword.text.toString())
                        CommonSharedPref.setSharedPrefDataByKey(this@LoginActivity, AppConstants.S_KEY_DRIVER_ID, jsonObj.optString("id"))

                        moveToTrips()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }
}
