package com.kairos.utils;

import java.util.ArrayList;

/**
 * Created by Naresh Ravva on 3/5/2021.
 */
public class KariosConstants {

    public static ArrayList<String> isSelectedTrips = new ArrayList<>();

    public static String LOC_TYPE = "LOC_TYPE";
    public static String FROM_LOC_NAME = "FROM_LOC_NAME";
    public static String TO_LOC_NAME = "FROM_LOC_NAME";
    public static int UPDATE_LOC_POSITION = 0;
    public static Boolean DO_REFRESH = false;
    public static Boolean DO_REFRESH_LIST = false;
}
