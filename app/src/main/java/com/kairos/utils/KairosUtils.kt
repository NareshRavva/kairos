package com.kairos.utils

import okhttp3.RequestBody
import org.json.JSONObject

/**
 * Created by Naresh Ravva on 5/20/2020.
 */
object KairosUtils {

    fun getRequestBody(objt: JSONObject): RequestBody {
        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), objt.toString())
    }

}