package com.kairos.utils

/**
 * Created by Naresh Ravva on 12/9/2019.
 */
class AppConstants {
    companion object {
        internal var selectedTrips: ArrayList<String>? = java.util.ArrayList()

        @kotlin.jvm.JvmField
        val S_KEY_TOKEN = "token"
        val S_KEY_IS_LOGIN = "0"
        val S_KEY_IS_FIRSTNAME = "FIRSTNAME"
        val S_KEY_IS_LASTNAME = "LASTNAME"
        val S_KEY_IS_EMAIL = "EMAIL"
        val S_KEY_IS_PW = "PASSWORD"
        val S_KEY_DRIVER_ID = "driveriD"
        var CUSTOMER_SIGNATURE = "customerSignature"
        var CUSTOMER_SIGNATURE_TIME = "customerSignatureTime"
    }
}