package com.kairos.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class CommonSharedPref {

    /**
     * @param context
     * @param key_name
     * @param value
     */
    public static void setSharedPrefDataByKey(Context context, String key_name, String value) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(key_name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(key_name, value);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @param key_name
     * @return
     */
    public static String getSharedPrefDataByKey(Context context, String key_name) {
        SharedPreferences appPref = context.getSharedPreferences(key_name, Context.MODE_PRIVATE);
        String value = appPref.getString(key_name, null);
        return value;
    }


}
