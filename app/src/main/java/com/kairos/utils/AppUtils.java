package com.kairos.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.kairos.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by naga on 30/05/2017.
 */
public class AppUtils {

    public static AlertDialog.Builder alertDialogBuilder = null;
    public static AlertDialog alertDialog = null;

    private static final String TAG = AppUtils.class.getName();

    private static ProgressDialog progressDialog = null;


    public static void customTitle(Context context, String title) {
        TextView tv = new TextView(context);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(lp);
        tv.setText(title);
        int dp = (int) (context.getResources().getDimension(R.dimen.title_size) / context.getResources().getDisplayMetrics().density);
        tv.setTextSize(dp);
        tv.setTextColor(context.getResources().getColor(R.color.G500));
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaPT-Demi.otf");
        tv.setTypeface(tf);
        ((AppCompatActivity) context).getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ((AppCompatActivity) context).getSupportActionBar().setCustomView(tv);
    }

    /**
     * close keyboard
     *
     * @param myEditText
     */
    public static void hideKeyBoard(Context ctx, EditText myEditText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(Context ctx, View v) {
        InputMethodManager in = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * show TOAST
     *
     * @param msg
     */
    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static void showAlert(Context context, String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setMessage(msg);
        builder.setInverseBackgroundForced(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * show DAILOG
     *
     * @param ctx
     */
    public static void showDialog(Context ctx) {
        if (ctx != null) {
            try {
                progressDialog = new ProgressDialog(ctx, ProgressDialog.STYLE_SPINNER);
                if (!progressDialog.isShowing()) progressDialog.show();
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.dialog_progress);
                progressDialog.setCancelable(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Dismiss DAILOG
     */
    public static void dismissDialog() {
        try {
            if (progressDialog != null) progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isNetConnected() {
        return isNetConnected(null);
    }

    public static boolean isNetConnected(Context context) {
        try {
            //does it matter which Context is used here? would applicationContext work for all cases?
            // getSystemService doesn't work on contexts with lifecycle state before onCreate.
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo activeNetworkInfo = connectivity.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }

        return false;
    }

    public static int getFontnName(int fonttype) {
        int fontName = 0;
        switch (fonttype) {
            case 1:
                fontName = R.string.Font_Regular;
                break;
            case 2:
                fontName = R.string.Font_Bold;
                break;
            case 3:
                fontName = R.string.Font_Light;
                break;
            case 4:
                fontName = R.string.Font_Medium;
                break;
            case 5:
                fontName = R.string.Font_RegularOblique;
                break;
            case 6:
                fontName = R.string.Font_BoldOblique;
                break;
            case 7:
                fontName = R.string.Font_LightOblique;
                break;
            case 8:
                fontName = R.string.Font_MediumOblique;
                break;
            default:
                break;
        }
        return fontName;
    }

    public static Typeface viewsCustomStyles(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        return Typeface.createFromAsset(context.getAssets(), "fonts/" + context.getResources().getString(getFontnName(a.getInteger(R.styleable.CustomFontTextView_fontName, 0))) + ".otf");
    }

    public static void handleActionBarBackBtn(Activity activity, MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            activity.finish();
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public static String parseDate(String time) {
        time = time.replace("Z", "");
//        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
//        String outputPattern = "EEE, d MMM yyyy";
//        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
//        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//        Date date = null;
//        String str = null;
//
//        try {
//            date = inputFormat.parse(time);
//            str = outputFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        try {
            Date dateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateTime);
            Calendar today = Calendar.getInstance();
            Calendar tomorrow = Calendar.getInstance();
            tomorrow.add(Calendar.DATE, +1);

            if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                return "Today";
            } else if (calendar.get(Calendar.YEAR) == tomorrow.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == tomorrow.get(Calendar.DAY_OF_YEAR)) {
                return "Tomorrow";
            } else {
                String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
                String outputPattern = "EEE, d MMM yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                Date date = null;
                String str = null;

                try {
                    date = inputFormat.parse(time);
                    str = outputFormat.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return str;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String parseTime(String time) {
        if(time!=null) {
            time = time.replace("Z", "");
            String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
            String outputPattern = "d MMM yyyy h:mm a";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
        return "";
    }

    public static String parseSigTime(String time) {
        if(time!=null) {
            time = time.replace("Z", "");
            String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
            String outputPattern = "HH:mm";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
        return "";
    }

    public static String currentTime() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = df.format(c);
        return formattedDate;
    }


}
