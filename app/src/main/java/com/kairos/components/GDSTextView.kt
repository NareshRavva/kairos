package com.kairos.components

import android.content.Context
import android.util.AttributeSet
import com.kairos.utils.AppUtils

/**
 * Created by Naresh Ravva on 9/3/2019.
 */

class GDSTextView : androidx.appcompat.widget.AppCompatTextView {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        style(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        style(context, attrs)
    }

    private fun style(context: Context, attrs: AttributeSet) {
        typeface = AppUtils.viewsCustomStyles(context, attrs)
    }
}