package com.kairos.components

import android.content.Context
import com.google.android.material.textfield.TextInputLayout
import android.util.AttributeSet
import com.kairos.utils.AppUtils

/**
 * Created by Naresh Ravva on 9/3/2019.
 */

class GDSTextInputLayout : TextInputLayout {

    internal var customFont: String? = null

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        style(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        style(context, attrs)
    }

    private fun style(context: Context, attrs: AttributeSet) {
        typeface = AppUtils.viewsCustomStyles(context, attrs)
    }
}