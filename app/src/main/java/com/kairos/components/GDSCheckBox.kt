package com.kairos.components

import android.content.Context
import android.util.AttributeSet

import com.kairos.utils.AppUtils

/**
 * Created by NKollu on 5/8/2018.
 */

class GDSCheckBox : androidx.appcompat.widget.AppCompatCheckBox {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        style(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        style(context, attrs)

    }

    private fun style(context: Context, attrs: AttributeSet) {
        typeface = AppUtils.viewsCustomStyles(context, attrs)
    }
}