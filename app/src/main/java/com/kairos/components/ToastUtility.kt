package com.kairos.components

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.kairos.R

/**
 * Created by Naresh Ravva on 9/5/2019.
 */

class ToastUtility(private val context: Context, private val inflater: LayoutInflater, private val strTitle: String, private val strMessage: String, private val type: TOAST_TYPE) : Runnable {

    override fun run() {
        try {
            val view = inflater.inflate(R.layout.list_item_custom_toast, null)

            val tvTitle: TextView = view.findViewById(R.id.tvTitle)
            val tvSubTitle: TextView = view.findViewById(R.id.tvSubTitle)
            val rlView: RelativeLayout = view.findViewById(R.id.rlView)
            val ivType: ImageView = view.findViewById(R.id.ivType)

            if (strTitle.isEmpty() || strTitle == null) {
                tvTitle.text = ""
                tvTitle.visibility = View.INVISIBLE
                tvSubTitle.setTextColor(ContextCompat.getColor(context, R.color.G500))
            } else {
                tvTitle.text = strTitle
            }

            tvSubTitle.text = strMessage

            when (type) {
                TOAST_TYPE.SUCCESS -> {
                    rlView.setBackgroundColor(ContextCompat.getColor(context, R.color.B300))
                    ivType.setImageResource(R.drawable.toast_success)
                }
                TOAST_TYPE.GENERIC -> {
                    rlView.setBackgroundColor(ContextCompat.getColor(context, R.color.C300))
                    ivType.setImageResource(R.drawable.flag_transparent)
                }
                TOAST_TYPE.ERROR -> {
                    rlView.setBackgroundColor(ContextCompat.getColor(context, R.color.P500))
                    ivType.setImageResource(R.drawable.ic_errortoast)
                }
            }

            val toast = Toast(context)
            toast.duration = Toast.LENGTH_LONG
            toast.setGravity(Gravity.BOTTOM, 0, 50)
            toast.view = view
            toast.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = ToastUtility::class.java.name
    }
}


enum class TOAST_TYPE {
    SUCCESS,
    GENERIC,
    ERROR
}