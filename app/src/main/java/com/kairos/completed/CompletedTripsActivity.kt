package com.kairos.completed

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kairos.LoginActivity
import com.kairos.R
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.tripDetail.TripDetailsActvity
import com.kairos.trips.TripLocationsModel
import com.kairos.trips.TripModel
import com.kairos.trips.TripsAdapter
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_completed_trips.*
import kotlinx.android.synthetic.main.app_bar_main.rvData
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response


/**
 * Created by Naresh Ravva on 5/6/2020.
 */
class CompletedTripsActivity : AppCompatActivity(), TripsAdapter.ItemClickListener {

    var trips: ArrayList<TripModel> = ArrayList()

    private var user_accounts_adapter: TripsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_completed_trips)

        getCompletedTripsData()

        rlBack.setOnClickListener{
            finish()
        }
    }

    private fun getCompletedTripsData() {
        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.COMPLETED_TRIPS
        val call = apiService.getCompletedTrips(service_url, token)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {

                    if (response.code() == 401) {
                        AppUtils.showToast(this@CompletedTripsActivity, "Session Expired")

                        //signout
                        CommonSharedPref.setSharedPrefDataByKey(this@CompletedTripsActivity, AppConstants.S_KEY_IS_LOGIN, "0")
                        moveToLoin()
                    } else if (response.code() == 404) {
                        AppUtils.showAlert(this@CompletedTripsActivity, "Invalid Email/Password")
                    } else if (response.code() == 200) {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)

                        val tripsArr = jsonObj.optJSONArray("tripsData")

                        if (tripsArr == null) {
                            setDataAdapter(ArrayList())
                            return
                        }

                        trips = ArrayList()
                        for (i in 0..tripsArr.length() - 1) {
                            val tripObjt = tripsArr.optJSONObject(i)

                            val id = tripObjt.optString("iTripID")
                            val date = tripObjt.optString("dtPickupDateTime")
                            val customerName = tripObjt.optString("cFirstName") + " " + tripObjt.optString("cLastName")
                            val mobile = tripObjt.optString("iMobileNumber")
                            var pickUpLocation = ""
                            var dropLocation = ""
                            val iOdometerStartReading = tripObjt.optInt("iOdometerEndReading")
                            val cCompanyName = tripObjt.optString("cCompanyName")

                            val tripDetailsArr = tripObjt.optJSONArray("tripDetails")

                            val arrLocation: ArrayList<TripLocationsModel> = ArrayList()
                            for (k in 0..tripDetailsArr.length() - 1) {
                                val tripDetailObjt = tripDetailsArr.optJSONObject(k)
                                pickUpLocation = tripDetailObjt.optString("cFrom")
                                dropLocation = tripDetailObjt.optString("cTo")

                                val tlModel = TripLocationsModel()
                                tlModel.iTripDetailsID = tripDetailObjt.optString("cFrom")
                                tlModel.iTripID = tripDetailObjt.optString("iTripID")
                                tlModel.cFrom = tripDetailObjt.optString("cFrom")
                                tlModel.cFromLatLng = tripDetailObjt.optString("cFromLatLng")
                                tlModel.cFromPlaceId = tripDetailObjt.optString("cFromPlaceId")
                                tlModel.cTo = tripDetailObjt.optString("cTo")
                                tlModel.cToLatLng = tripDetailObjt.optString("cToLatLng")
                                tlModel.cToPlaceId = tripDetailObjt.optString("cToPlaceId")
                                tlModel.iOrder = tripDetailObjt.optString("iOrder")
                                tlModel.dtPickupDateTime = tripDetailObjt.optString("dtPickupDateTime")
                                tlModel.dtDropDateTime = tripDetailObjt.optString("dtDropDateTime")
                                arrLocation.add(tlModel)
                            }

                            val tripModel = TripModel()
                            tripModel.id = id
                            tripModel.date = date
                            tripModel.pickupLocation = pickUpLocation
                            tripModel.dropLocation = dropLocation
                            tripModel.customerName = customerName
                            tripModel.mobile = mobile
                            tripModel.iOdometerStartReading = iOdometerStartReading
                            tripModel.arrLocations = arrLocation
                            tripModel.cCompanyName = cCompanyName
                            trips.add(tripModel)
                        }

                        setDataAdapter(trips)

                        Log.v("DATA", "trips-->" + trips.size)
                    }else if (response.code() == 503) {
                        AppUtils.showAlert(this@CompletedTripsActivity, getString(R.string.server_down_message))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun moveToLoin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun setDataAdapter(list: java.util.ArrayList<TripModel>) {
        if (list.size > 0) {
            rvData.setLayoutManager(LinearLayoutManager(this))
            user_accounts_adapter = TripsAdapter(this, list, true)
            user_accounts_adapter!!.setClickListener(this)
            rvData.setAdapter(user_accounts_adapter)
            llNoData.setVisibility(View.GONE)
            rvData.setVisibility(View.VISIBLE)
        } else {
            llNoData.setVisibility(View.VISIBLE)
            rvData.setVisibility(View.GONE)
        }
    }

    override fun itemClick(view: View?, position: Int, type: String?) {
        val intent = Intent(this, TripDetailsActvity::class.java)
        startActivity(intent)
    }
}