package com.kairos.expenses

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kairos.R
import com.kairos.api.ApiClientJSON
import com.kairos.api.ApiInterface
import com.kairos.trips.TripModel
import com.kairos.utils.AppConstants
import com.kairos.utils.AppUtils
import com.kairos.utils.CommonSharedPref
import com.mikelau.croperino.Croperino
import com.mikelau.croperino.CroperinoConfig
import com.mikelau.croperino.CroperinoFileUtil
import com.tax.api.ServiceUrl
import kotlinx.android.synthetic.main.activity_add_expenses.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

/**
 * Created by Naresh Ravva on 5/24/2020.
 */
class AddExpenses : AppCompatActivity() {

    var filePath: File = File("")

    var HAVING_FILE_DATA = 0

    var arr_ids = ArrayList<String>()
    var arr_types = ArrayList<String>()

    var EXP_TYPE = "2"

    var tripModel = TripModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_expenses)

        getCategories()

        tripModel = intent.getSerializableExtra("TRIP_DATA") as TripModel

        rlImageUpload.setOnClickListener {
            //Initialize on every usage
            CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/MikeLau/Pictures", "/sdcard/MikeLau/Pictures")
            CroperinoFileUtil.verifyStoragePermissions(this@AddExpenses)
            CroperinoFileUtil.setupDirectory(this@AddExpenses)
            //Prepare Camera
            Croperino.prepareCamera(this@AddExpenses)
        }

        rlBack.setOnClickListener {
            finish()
        }

        btnSubmit.setOnClickListener {
            uploadFile(filePath)
        }
    }

    private fun getCategories() {
        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        val apiService = ApiClientJSON.getClient(this).create(ApiInterface::class.java)
        val service_url = ServiceUrl.BASE_URL + ServiceUrl.EXPENSE_TYPES
        val call = apiService.getExpenseTypes(service_url, token)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.code() == 503) {
                        AppUtils.showAlert(this@AddExpenses, getString(R.string.server_down_message))
                    }else {
                        val res = response.body()!!.string()
                        val jsonObj = JSONObject(res)
                        val expArr = jsonObj.optJSONArray("data")

                        for (i in 0..expArr.length() - 1) {
                            val expObject1 = expArr.optJSONObject(0)
                            EXP_TYPE = expObject1.optString("iExpenseTypeID")

                            val expObject = expArr.optJSONObject(i)
                            arr_ids.add(expObject.optString("iExpenseTypeID"))
                            arr_types.add(expObject.optString("cExpenseType"))
                        }

                        setSpinnerData(arr_types)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

    private fun setSpinnerData(arrTypes: ArrayList<String>) {
        spinner.setItems(arrTypes)
        spinner.setOnItemSelectedListener { view, position, id, item ->
            EXP_TYPE = arr_ids.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Do saving / uploading of photo method here.
        //The image file can always be retrieved via CroperinoFileUtil.getTempFile()
        when (requestCode) {
            CroperinoConfig.REQUEST_TAKE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    /* Parameters of runCropImage = File, Activity Context, Image is Scalable or Not, Aspect Ratio X, Aspect Ratio Y, Button Bar Color, Background Color */
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@AddExpenses, true, 1, 1, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, this@AddExpenses);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this@AddExpenses, true, 0, 0, R.color.gray, R.color.gray_variant)
                }
            }
            CroperinoConfig.REQUEST_CROP_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val i = Uri.fromFile(CroperinoFileUtil.getTempFile())
                    imgInvoice.setImageURI(i)
                    HAVING_FILE_DATA = 1
                    filePath = File(i.getPath())
                    //Do saving uploading of photo method here. The image file can always be retrieved via CroperinoFileUtil.getTempFile()
                }
            }
        }
    }

    private fun uploadFile(file: File) {

        if (inputPrice.text.toString().trim().isEmpty()) {
            AppUtils.showAlert(this@AddExpenses, "Enter Price")
            return
        }
//         Client Don't want this condition
//        if (HAVING_FILE_DATA == 0) {
//            AppUtils.showAlert(this@AddExpenses, "Upload Invoice")
//            return
//        }

        AppUtils.showDialog(this@AddExpenses)



        val apiService: ApiInterface = ApiClientJSON.getClient(this).create(ApiInterface::class.java)

        val tripId = RequestBody.create(MediaType.parse("text/plain"), tripModel.id)
        val expenseTypeId = RequestBody.create(MediaType.parse("text/plain"), EXP_TYPE)
        val cost = RequestBody.create(MediaType.parse("text/plain"), inputPrice.text.toString())

        val token = CommonSharedPref.getSharedPrefDataByKey(this, AppConstants.S_KEY_TOKEN)

        var call = apiService.addExpense(token, tripId, expenseTypeId, cost,null)
        if (HAVING_FILE_DATA == 1) {
            val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
            val fileToUpload = MultipartBody.Part.createFormData("photo", file.name, requestBody)
            call = apiService.addExpense(token, tripId, expenseTypeId, cost, fileToUpload)
        }

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val res = response.body()!!.string()
                    val jsonObj = JSONObject(res)

                    if (response.code() == 200) {
                        val intent = Intent()
                        intent.putExtra("REFRESH_DATA", true)
                        setResult(Activity.RESULT_OK, intent);
                        finish()
                    }
                    else if (response.code() == 503) {
                        AppUtils.showAlert(this@AddExpenses, getString(R.string.server_down_message))
                    }else {
                        AppUtils.showAlert(this@AddExpenses, "Fail to Submit Invoice, Please Try again later")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    AppUtils.dismissDialog()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                AppUtils.dismissDialog()
            }
        })
    }

}