package com.kairos.expenses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kairos.R;
import com.kairos.tripDetail.ExpenesesDataModel;
import com.kairos.utils.AppUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ViewHolder> {

    ArrayList<ExpenesesDataModel> arrList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private static final String TAG = ExpensesAdapter.class.getName();
    private Boolean isFlag;

    public ExpensesAdapter(Context context, ArrayList<ExpenesesDataModel> arrList,Boolean isFlag) {
        this.mInflater = LayoutInflater.from(context);
        this.arrList = arrList;
        this.context = context;
        this.isFlag = isFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_expense, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ExpenesesDataModel model = arrList.get(position);

        if (model.getCExpenseImage() != null && model.getCExpenseImage().length() > 10) Picasso.get().load(model.getCExpenseImage()).into(holder.imgExp);
        else holder.imgExp.setImageResource(R.drawable.no_image_1);

        holder.txtExpCost.setText("₱" + model.getIExpenseCost());
        holder.txtExpType.setText(model.getCExpenseType());
        holder.txtExpDate.setText(AppUtils.parseTime(model.getDatetime()));

        //if (model.getCExpenseType().equalsIgnoreCase("Fuel")) holder.imgExpType.setImageResource(R.drawable.fuel_1);
        if (model.getCExpenseType().equalsIgnoreCase("Toll fee")) holder.imgExpType.setImageResource(R.drawable.toll_road);
        else if (model.getCExpenseType().equalsIgnoreCase("Parking Fee")) holder.imgExpType.setImageResource(R.drawable.parking);
        else if (model.getCExpenseType().equalsIgnoreCase("Driver Meals")) holder.imgExpType.setImageResource(R.drawable.lunch);
        else holder.imgExpType.setImageResource(R.drawable.ic_miscellaneous);

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.itemClick(view, model ,"");
            }
        });

        if(isFlag) holder.llDelete.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtExpCost, txtExpType, txtExpDate,txtDelete;
        public ImageView imgExp, imgExpType;
        public LinearLayout llDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            imgExp = itemView.findViewById(R.id.imgExp);
            imgExpType = itemView.findViewById(R.id.imgExpType);
            txtExpCost = itemView.findViewById(R.id.txtExpCost);
            txtExpType = itemView.findViewById(R.id.txtExpType);
            txtExpDate = itemView.findViewById(R.id.txtExpDate);
            txtDelete = itemView.findViewById(R.id.txtDelete);
            llDelete = itemView.findViewById(R.id.llDelete);
        }

        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.itemClick(view, getAdapterPosition(), "");
        }
    }

    public ExpenesesDataModel getItem(int id) {
        return arrList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void itemClick(View view, ExpenesesDataModel model, String type);
    }
}