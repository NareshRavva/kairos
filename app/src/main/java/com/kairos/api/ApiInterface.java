package com.kairos.api;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST()
    Call<ResponseBody> login_authenticate(@Url String url, @Body RequestBody params);

    @GET()
    Call<ResponseBody> getTrips(@Url String url, @Header("Authorization") String auth);

    @GET()
    Call<ResponseBody> getCompletedTrips(@Url String url, @Header("Authorization") String auth);

    @GET()
    Call<ResponseBody> getExpenseTypes(@Url String url, @Header("Authorization") String auth);

    @POST()
    Call<ResponseBody> getExpenseList(@Url String url, @Header("Authorization") String auth,@Body RequestBody params);

    @Multipart
    @POST("api/v1/addExpense")
    Call<ResponseBody> addExpense(@Header("Authorization") String auth,
                                  @Part("tripId") RequestBody tripId,
                                  @Part("expenseTypeId") RequestBody expenseTypeId,
                                  @Part("cost") RequestBody cost, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/v1/endTrip")
    Call<ResponseBody> endTrip(@Header("Authorization") String auth,
                                  @Part("tripId") RequestBody tripId,
                                  @Part("reading") RequestBody expenseTypeId,
                                  @Part("orderId") RequestBody cost,
                                  @Part MultipartBody.Part file);

    @Multipart
    @POST("api/v1/updateEndTripStatus")
    Call<ResponseBody> updateEndTripStatus(@Header("Authorization") String auth,
                               @Part("tripId") RequestBody tripId,
                               @Part MultipartBody.Part signfile);


    @Multipart
    @POST("api/v1/startTrip")
    Call<ResponseBody> startTrip(@Header("Authorization") String auth,
                                  @Part("tripId") RequestBody tripId,
                                  @Part("reading") RequestBody reading,
                                  @Part("orderId") RequestBody cost,
                                  @Part MultipartBody.Part file);

    @POST()
    Call<ResponseBody> addDestination(@Url String url, @Header("Authorization") String auth, @Body RequestBody params);


    @POST()
    Call<ResponseBody> updateDestination(@Url String url, @Header("Authorization") String auth, @Body RequestBody params);


    @POST()
    Call<ResponseBody> updateFirebaseToken(@Url String url, @Header("Authorization") String auth,@Body RequestBody params);


    @POST()
    Call<ResponseBody> deleteExpenses(@Url String url, @Header("Authorization") String auth,@Body RequestBody params);

}
