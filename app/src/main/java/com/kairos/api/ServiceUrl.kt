package com.tax.api

/**
 * Created by Naresh Ravva on 12/10/2019.
 */
object ServiceUrl {

    var BASE_URL: String = "https://portal.kairoscarrental.com/api/"

    var LOGIN = "v1/login"
    var MY_TRIPS = "v1/trips"
    var COMPLETED_TRIPS = "v1/completedTrips"
    var EXPENSE_TYPES = "v1/expenseTypes"
    var GET_EXPENSES = "v1/getExpenses"
    var ADD_DESTINATION = "v1/addTripDestination"
    var UPDATE_DESTINATION = "v1/updateTripDestinations"
    var UPDATE_FIREBASE_TOKEN = "v1/updateDeviceID"
    var DELETE_EXPENSE = "v1/deleteExpenses"
}