package com.kairos.api;


import android.content.Context;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientJSON {

    private static Retrofit retrofit = null;
    static OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();


    public static Retrofit getClient(Context ctx) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://portal.kairoscarrental.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}


