package com.kairos

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_contactus.*
import kotlinx.android.synthetic.main.activity_webview.*
import kotlinx.android.synthetic.main.activity_webview.rlBack

/**
 * Created by Naresh Ravva on 10/24/2020.
 */
public class WebViewActivity :AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        webview.settings.setJavaScriptEnabled(true)

        txtTitle.setText(intent.getStringExtra("TITLE"))

        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        webview.loadUrl(intent.getStringExtra("URL"))

        rlBack.setOnClickListener {
            finish()
        }
    }
}