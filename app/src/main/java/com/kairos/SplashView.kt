package com.kairos

import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.os.Handler

/**
 * Created by Naresh Ravva on 4/25/2019.
 */

class SplashView : Activity() {

    var SPLASH_DISPLAY_LENGTH: Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        getDeviceWidthAndHeigth()

        Handler().postDelayed({
            moveToNextScreen(Intent(this, LoginActivity::class.java))
        }, SPLASH_DISPLAY_LENGTH)

    }

    private fun moveToNextScreen(intent: Intent) {
        startActivity(intent)
        finish()
    }

    private fun getDeviceWidthAndHeigth() {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        //Constants.DEVICE_DISPLAY_WIDTH = size.x
    }
}